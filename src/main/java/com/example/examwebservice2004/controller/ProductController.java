package com.example.examwebservice2004.controller;

import com.example.examwebservice2004.entity.Product;
import com.example.examwebservice2004.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1")
@RestController
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "/products")
    public List<Product> getAllProducts(){
        return (List<Product>) productRepository.findAll();
    }

    @PostMapping("/products")
    public Product addProduct(@RequestBody Product p){
        return productRepository.save(p);
    }

    @GetMapping("/products/sell/{id}/{quantity}")
    public Product sellProduct(@PathVariable int id,@PathVariable int quantity) {
        Product productSell = productRepository.findById(id).get();
        productSell.setQuantity(productSell.getQuantity() - quantity);
        return productRepository.save(productSell);
    }

}
